import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class BaiTapBuoi4Array {
    public static void main(String[] args) {
        bai1();
        bai2();
        bai3();
        bai6();
        Bai7();
        Bai9();
        Bai7();
        bai10();
        Bai11();
        bai12();
        bai13();
        Bai16();
        Bai17();
        Bai23();
        Bai27();
        Bai41();
        Bai59();
        Bai60();
    }



    private static void bai1() {
        //1. tạo class student(id, name, major), constructor 3 tham số
        Student sv1 = new Student(5, "tung", "java");
        Student sv2 = new Student(9, "anh", ".Net");
        Student sv3 = new Student(3, "bao", "JavaScript");
        Student sv4 = new Student(6, "hieu", "java");
        Student sv5 = new Student(5, "Hai", "java");
        Student sv6 = new Student(4, "Liem", "java");
        Student[] students = {sv1, sv2, sv3, sv4, sv5, sv6};
        //Sap xep

        Student SVtemp = new Student();

        int n = students.length;

        for (int i = 0; i < students.length - 1; i++) { //Loop once for each element in the array.
            for (int j = 0; j < students.length - 1 - i; j++) { //Once for each element, minus the counter.
//                    students[j].getId() > students[j+1].getId()
                if (students[j].getName().compareToIgnoreCase(students[j + 1].getName()) > 0) { //Test if need a swap or not.
                    SVtemp = students[j]; //These three lines just swap the two elements:
                    students[j] = students[j + 1];
                    students[j + 1] = SVtemp;
                }
            }
        }


        System.out.println(students.length);
        for (int i = 0; i < students.length; i++) {
            System.out.print(students[i].getName() + " ");
        }
    }

    private static void bai2() {
        //Tính tổng tất cả các giá trị của bảng
        int[] a = {1, 3, 5};
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        System.out.println();
        System.out.println(sum);
    }

    private static void bai3() {
        // Thêm 1 student vào mảng , Nếu trùng id thì k thêm được
        Student sv1 = new Student(5, "tung", "java");
        Student sv2 = new Student(9, "anh", ".Net");
        Student sv3 = new Student(3, "bao", "JavaScript");
        Student sv4 = new Student(6, "hieu", "java");
        Student sv5 = new Student(5, "Hai", "java");
        Student sv6 = new Student(4, "Liem", "java");
        Student[] students = {sv1, sv2, sv3, sv4, sv5, sv6};

        Student sv7 = new Student(10, "Liem", "java");
        int count = 0;
        for (int i = 0; i < students.length; i++) {
            if (sv7.getId() == students[i].getId()) {
                count++;

            }
        }
        if (count > 0) {
            System.out.println("trung id");
        } else {
            int l = students.length;
            students = Arrays.copyOf(students, l + 2);
            students[l + 1] = sv7;
        }
        System.out.println(students[7].getId());
    }

    private static void bai6() {
        //4: Tìm index của 1 phần tử trong mảng
        Student sv1 = new Student(5, "tung", "java");
        Student sv2 = new Student(9, "anh", ".Net");
        Student sv3 = new Student(3, "bao", "JavaScript");
        Student sv4 = new Student(6, "hieu", "java");
        Student sv5 = new Student(5, "Hai", "java");
        Student sv6 = new Student(4, "Liem", "java");
        Student[] students = {sv1, sv2, sv3, sv4, sv5, sv6};
        Student sv7 = new Student(4, "Liem", "java");
        for (int i = 0; i < students.length; i++) {
            if (sv7.getId() == students[i].getId()) {
                System.out.println(i);
                break;
            }
        }
    }

    private static void Bai7() {
        //5: Xóa 1 phần tử cho trước trong mảng
        Student sv1 = new Student(5, "tung", "java");
        Student sv2 = new Student(9, "anh", ".Net");
        Student sv3 = new Student(3, "bao", "JavaScript");
        Student sv4 = new Student(6, "hieu", "java");
        Student sv5 = new Student(4, "Hai", "java");
        Student sv6 = new Student(4, "Liem", "java");
        Student[] students = {sv1, sv2, sv3, sv4, sv5, sv6};
        Student sv7 = new Student(4, "Liem", "java");
        int c = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].getId() != sv7.getId()) {
                students[c] = students[i];
                c++;
            }
        }
        int l = students.length;
        l = c;
        for (int i = 0; i < l; i++) {
            System.out.print(students[i].getName() + " ");
        }
    }

    private static void Bai9() {
        // Thêm 1 phần tử vào vị trí cho sẵn
        Student sv1 = new Student(5, "tung", "java");
        Student sv2 = new Student(9, "anh", ".Net");
        Student sv3 = new Student(3, "bao", "JavaScript");
        Student sv4 = new Student(6, "hieu", "java");
        Student sv5 = new Student(7, "Hai", "java");
        Student sv6 = new Student(8, "hien", "java");
        Student[] students = new Student[10];
        Student sv7 = new Student(4, "Liem", "java");
        students = new Student[]{sv1, sv2, sv3, sv4, sv5, sv6};
        Student svTemp = new Student();
        int l = students.length;
        students = Arrays.copyOf(students, l + 1);
        System.out.println(students.length);
        int position = 1;
        for (int i = students.length - 1; i > position; i--) {
            students[i] = students[i - 1];
        }
        students[position] = sv7;
        for (int i = 0; i < l + 1; i++) {
            System.out.print(students[i].getName() + " ");
        }
    }

    private static void bai10() {
        // tim max min cua cac phan tu trong mang
        int[] a = {5, 9, 0, 10, 4};
        int max = a[0];
        int min = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
            }
            if (a[i] < min) {
                min = a[i];
            }
        }
        System.out.println("\nmax: " + max + " min: " + min);
    }

    private static void Bai11() {
        //Đảo ngược các phần tử trong mảng int
//        System.out.println();
        int[] a = {1, 2, 3, 4};
        for (int i = a.length - 1; i >= 0; i--) {
            System.out.print(+a[i] + " ");
        }
    }

    private static void bai12() {
        //Tìm phần tử xuất hiện nhiều lần
        int a[] = {1, 2, 3, 4, 2, 4, 6, 5, 1, 4};
        int b[] = new int[10];
        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                if (a[j] == a[i]) {
                    b[i] += 1;
                }
            }
        }
        System.out.println();
        for (int i = 0; i < b.length; i++) {
            if (b[i] == 2) {
                System.out.print(a[i] + " ");
//                break;
            }
        }
    }

    private static void bai13() {
        //Tìm phần tử xuất hiện nhiều lần
        String[] s = {"tung", "tung", "hien", "tung", "tung"};
        int[] s1 = new int[10];
        for (int i = 0; i < s.length; i++) {
            for (int j = i; j < s.length; j++) {
                if (s[i].compareToIgnoreCase(s[j]) == 0) {
                    s1[i] += 1;
                }
            }
        }
        System.out.println();
        for (int i = 0; i < s.length; i++) {
            if (s1[i] == 2) {
                System.out.println(s[i]);
            }
        }
    }

    private static void Bai16() {
        //Xóa phần tử xuất hiện nhiều lần

    }

    private static void Bai17() {
        //Tìm phần tử lớn thứ 2
        int[] a = {5, 9, 0, 10, 4};
        int max = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        int max1 = a[2];
        for (int i = 0; i < a.length; i++) {
            if (a[i] != max) {
                if (a[i] > max1) {
                    max1 = a[i];
                }
            }
        }
        System.out.println("\nmax1: " + max1);
    }

    private static void Bai23() {
        // So sánh 2 mảng

        int[] a = {5, 9, 0, 10, 4, 10};
        int[] b = {5, 9, 0, 10, 5, 9};
        if (a.length == b.length) {
            int dem = 0;
            for (int i = 0; i < a.length; i++) {
                if (a[i] == b[i]) {
                    dem++;
                }
            }
            if (dem == a.length) {
                System.out.printf("2 mang bang nhau");
            } else System.out.println("2 mang khac nhau!");

        } else System.out.println("2 mang khac nhau");
    }

    private static void Bai27() {
        //Tim so chan va so le
        int[] a = {5, 9, 0, 10, 4, 4};
        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.println("So chan: " + a[i]);
            } else System.out.println("So le: " + a[i]);
        }
    }

    private static void Bai41() {
        //Tim min va sencond min
        int[] a = {5, 9, 0, 10, 4};
        int min = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] < min) {
                min = a[i];
            }
        }
        int min1 = a[1];
        for (int i = 0; i < a.length; i++) {
            if (a[i] != min) {
                if (a[i] < min1) {
                    min1 = a[i];
                }
            }
        }
        System.out.println("\nmin: " + min + " Sencond min: " + min1);
    }

    private static void Bai59() {

        //Tim min va sencond min
        int[] a = {5, 9, 0, 10, 4};
        int max = a[0];
        for (int i = 0; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
            }
        }
        int max1 = a[1];
        for (int i = 0; i < a.length; i++) {
            if (a[i] != max) {
                if (a[i] > max) {
                    max1 = a[i];
                }
            }
        }
        System.out.println("tich maximum: " + (max1 * max));
    }
    private static void Bai60() {
        // in ra array xáo trộn
        int[] a = {5, 9, 0, 10, 4};
        for (int i = 0 ; i< a.length;i++){
            Random r = new Random();
            int j = r.nextInt(a.length);
            System.out.print(a[j]+" ");
        }
    }


}
